https://fosi.vn

FOSI (INTERNATIONAL FOOD SERVICES) – Một đơn vị tư vấn uy tín và chuyên nghiệp hàng đầu trong lĩnh vực tư vấn Thực phẩm tại Việt Nam. Với hơn 10 năm kinh nghiệm cùng đội ngũ chuyên gia tư vấn giàu kinh nghiệm và nhiệt huyết.

Công Ty Cổ Phần Dịch Vụ Thực Phẩm Quốc Tế Fosi
Đại diện pháp luật : Trần Quang Hải
470 Lê Thị Riêng, P.Thới An, Q.12, Tp.HCM
02866827330 – (028) 6682 7350
info@fosi.vn

follow us:
https://twitter.com/fosi_vn
https://www.reddit.com/user/fosivn
https://www.pinterest.com/fosivnvn
https://www.instagram.com/fosi.vn/
https://www.youtube.com/channel/UCpxu3a1ZrFvhJgynT_2QQbA
https://www.facebook.com/fosi.vn
https://www.linkedin.com/in/fosi-vn-65053820a/
